from __future__ import unicode_literals

from django.apps import AppConfig


class Config(AppConfig):
    name = 'dp_file_system_file'
