# Generated by Django 2.2.6 on 2019-10-07 21:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FSFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('creation_date', models.DateTimeField(auto_now_add=True, null=True)),
                ('modification_date', models.DateTimeField(auto_now=True, null=True)),
                ('size', models.IntegerField(null=True)),
                ('mime_type', models.CharField(max_length=100)),
                ('path', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'dp_file_system_file',
            },
        ),
    ]
