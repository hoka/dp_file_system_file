from .settings import INSTALLED_APPS as BASE_INSTALLED_APPS

DEBUG = True

INSTALLED_APPS = BASE_INSTALLED_APPS + \
                 [
                     'dp_file_system_file'
                 ]
